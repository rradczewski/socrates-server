// @flow

import express from 'express';
import Authenticator from '../../../domain/authenticator';

export default class AuthenticationApiVersion1 extends express.Router {
  authenticator: Authenticator;

  constructor(authenticator: Authenticator) {
    super();
    this.authenticator = authenticator;

    this.post('/', (req: express.Request, res: express.Response) => {
      this.authenticator.login(req.body.email, req.body.password)
        .then(result => {
          res.status(200).send(result);
        })
        .catch(() => {
          res.status(200).send({success: false, token: '', isAdministrator: false});
        });
    });
  }
}



