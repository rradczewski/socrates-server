// @flow

import express from 'express';

export default class RootApiVersion1 extends express.Router {
  constructor() {
    super();

    this.get('/', (req: express.Request, res: express.Response) => {
      res.status(200).json({'status': 'API v1 UP'}).end();
    });
  }
}



