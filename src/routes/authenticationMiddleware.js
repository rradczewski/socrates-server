// @flow
import express from 'express';
import {verify} from 'jsonwebtoken';
import AuthenticationRepository from '../db/authenticationRepository';
import Authenticator, {JSONWebTokenSecret} from '../domain/authenticator';
import AuthenticationApiVersion1 from './api/v1/authenticationApi';
import type {Middleware} from '../middleware';
import MySql from '../db/MySql';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {AccessCheckResult} from '../domain/authenticator';

function isJWTAuthorizationAvailable(req) {
  return req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT';
}

function extractJWTToken(req) {
  return req.headers.authorization.split(' ')[1];
}

function populateRequestUser(req) {
  if (isJWTAuthorizationAvailable(req)) {
    verify(extractJWTToken(req), JSONWebTokenSecret, (err, decode) => {
      req.user = err ? undefined : decode;
    });
  } else {
    req.user = undefined;
  }
  console.log(req.user);
}

export default function authenticationMiddleware(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const authenticator = new Authenticator(new AuthenticationRepository(sqlHandler), dispatcher);
  return (app: express.Application) => {
    app.use(async (req: express.Request, res: express.Response, next: express.NextFunction) => {
      populateRequestUser(req);
      const access: AccessCheckResult = await authenticator.checkAccess(req.url, req.user);
      if(access.allowed) {
        return next();
      } else {
        res.status(401).send(access.error);
      }
    });
    app.use('/api/v1/login/', new AuthenticationApiVersion1(authenticator));
  };
}