// @flow
import express from 'express';
import NewsletterApiVersion1 from './api/v1/newsletterApi';
import RootApiVersion1 from './api/v1/rootApi';
import NewsletterReadModel from '../domain/newsletterReadModel';
import NewsletterRepository from '../db/newsletterRepository';
import Newsletter from '../domain/newsletter';
import MySql from '../db/MySql';
import type {EventDispatcher} from '../domain/eventDispatcher';
import type {Middleware} from '../middleware';

export default function createNewsletterServer(sqlHandler: MySql, dispatcher: EventDispatcher): Middleware {
  const newsletter = new Newsletter(new NewsletterRepository(sqlHandler), dispatcher);
  const newsletterReadModel = new NewsletterReadModel(dispatcher);
  return (app: express.Application) => {
    app.use('/api/v1/', new RootApiVersion1());
    app.use('/api/v1/', new NewsletterApiVersion1(newsletter, newsletterReadModel));
  };
}