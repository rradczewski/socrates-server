// @flow

import express from 'express';
import bodyParser from 'body-parser';

export default function bodyParsers(app: express.Application): void {
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: false}));

}