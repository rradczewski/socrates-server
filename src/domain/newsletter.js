// @flow
import Aggregate from './aggregate';
import {EventDispatcher} from './eventDispatcher';
import NewsletterRepository from '../db/newsletterRepository';
import {validateEmail} from '../utilities/Utilities';

export type NewsletterSubscriber = {
  name: string,
  email: string
}

export default class Newsletter extends Aggregate {
  _repository: NewsletterRepository;

  constructor(repository: NewsletterRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  async signup(name: string, email: string): Promise<NewsletterSubscriber> {
    validatePerson(name, email);
    const person = await this._repository.addPerson(name, email);
    if (person) {
      this.dispatch({type: 'INTERESTED_PERSON_CREATED', payload: person});
      return person;
    } else {
      throw new Error('Registration failed. Interested person could not be added.');
    }
  }

  async unsubscribe(email: string): Promise<void> {
    console.log('unsubscribing ' + email);
    // TODO: Implementation missing
  }
}

function validatePerson(name: string, email: string) {
  if (!name) {
    throw new Error('The information provided is not valid: The name is missing.');
  } else if (!email) {
    throw new Error('The information provided is not valid: The email address is missing.');
  } else if (!validateEmail(email)) {
    throw new Error('The information provided is not valid: The email address is malformed.');
  }
}