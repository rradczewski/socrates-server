// @flow

import DomainEventDispatcher from './domainEventDispatcher';
import MySql from '../db/MySql';
import TestHelper from '../db/SqlTestHelper';
import Authenticator from './authenticator';
import AuthenticationRepository from '../db/authenticationRepository';
import type {AccessCheckResult, LoginResult} from './authenticator';

jest.mock('../db/authenticationRepository');

describe('Authenticator', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, authenticator, authenticationRepository;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    authenticationRepository = new AuthenticationRepository(mySql);
    authenticator = new Authenticator(authenticationRepository, dispatcher);
  });

  const theHash = '$2a$10$eEhkyHRz6dtEQEHnQdDkher2Lg2ZrzQOC2fxiw7vi8naXZIqHZ.re';
  describe('when logging in', () => {
    describe('and right email and password are provided', () => {

      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it('return a result containing a token', () => {
        return authenticator.login('test@test.org', 'myPassword')
          .then((result: LoginResult) => {
            expect(result.token !== '').toBe(true);
          });
      });

      it('dispatch USER_LOGGED_IN', async (done) => {
        dispatcher.subscribeOnce('USER_LOGGED_IN', () => done());
        await authenticator.login('test@test.org', 'myPassword');
      });
    });
    describe('and false password is provided', () => {

      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it('return a result containing an empty token', () => {
        return authenticator.login('test@test.org', 'anotherPassword')
          .then((result: LoginResult) => {
            expect(result.token === '').toBe(true);
          });
      });

      it('dispatch USER_LOGIN_FAILURE', async (done) => {
        dispatcher.subscribeOnce('USER_LOGIN_FAILURE', () => done());
        await authenticator.login('test@test.org', 'anotherPassword');
      });
    });
    describe('and no user for the email can be found', () => {

      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it('return empty token', async () => {
        const result = await authenticator.login('nonexistentEmail@valid.com', 'somePassword');
        expect(result.token === '').toBe(true);
      });
    });
    describe('with invalid request data', () => {

      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });

      it(' if no email return a failure result with an empty token', async () => {
        const result = await authenticator.login('', 'anotherPassword');
        expect(result.token === '').toBe(true);
      });
      it('if no password return a failure result', async () => {
        const result = await authenticator.login('test@test.org', 'a');
        expect(result.token === '').toBe(true);
      });
      it('if invalid email return a failure result', async () => {
        const result = await authenticator.login('invalidEmail', 'anotherPassword');
        expect(result.token === '').toBe(true);
      });
    });
  });

  describe('when checking access', () => {
    describe('to a non secured destination', () => {
      it('from a non-logged in user access is allowed', async () => {
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/nonsecured/', undefined);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
      it('from a logged in user access is allowed', async () => {
        const user = {name: 'test', email: 'test@test.org', password: theHash, isAdministrator: false};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/nonsecured/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    it('to a destination that requires authentication from a non-logged in user access is denied', async () => {
      const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/profile/', undefined);
      expect(result.allowed).toBe(false);
      expect(result.error).toBeDefined();
      if(result.error) {
        expect(result.error.message).toEqual('Authentication required.');
      }
    });
    describe('to a destination that requires authentication from a verifiable logged in user ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash, isAdministrator: false}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is allowed', async () => {
        const user = {name: 'test', email: 'test@test.org', password: '', isAdministrator: false};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/profile/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    describe('to a destination that requires authentication from a non verifiable logged in user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {name: 'test', email: 'test@test.org', password: theHash, isAdministrator: false};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/profile/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if(result.error) {
          expect(result.error.message).toEqual('Authentication required.');
        }
      });
    });
    it('to a destination that requires an administrator from a non-logged in user access is denied', async () => {
      const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', undefined);
      expect(result.allowed).toBe(false);
      expect(result.error).toBeDefined();
      if(result.error) {
        expect(result.error.message).toEqual('Authentication required.');
      }
    });
    describe('to a destination that requires an administrator from a verifiable logged in non administrator ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash, isAdministrator: false}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {name: 'test', email: 'test@test.org', password: '', isAdministrator: false};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if(result.error) {
          expect(result.error.message).toEqual('Administrator required.');
        }
      });
    });
    describe('to a destination that requires an administrator from a verifiable logged in administrator ', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(
            {name: 'test', email: 'test@test.org', password: theHash, isAdministrator: true}
          );
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is allowed', async () => {
        const user = {name: 'test', email: 'test@test.org', password: '', isAdministrator: false};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', user);
        expect(result.allowed).toBe(true);
        expect(result.error).toBeNull();
      });
    });
    describe('to a destination that requires an administrator from a non verifiable logged in user', () => {
      beforeEach(() => {
        authenticationRepository.readPersonByEmail.mockImplementation(() => {
          return Promise.resolve(null);
        });
      });
      afterEach(() => {
        authenticationRepository.readPersonByEmail.mockReset();
      });
      it('access is denied', async () => {
        const user = {name: 'test', email: 'test@test.org', password: theHash, isAdministrator: false};
        const result: AccessCheckResult = await authenticator.checkAccess('/ap1/v1/management/', user);
        expect(result.allowed).toBe(false);
        expect(result.error).toBeDefined();
        if(result.error) {
          expect(result.error.message).toEqual('Authentication required.');
        }
      });
    });
  });
});
