// @flow
import {EventDispatcher, Event} from './eventDispatcher';
import type {NewsletterSubscriber} from './newsletter';

export default class NewsletterReadModel {
  _dispatcher: EventDispatcher;
  _newsletterSubscribers: NewsletterSubscriber[];

  constructor(dispatcher: EventDispatcher) {
    this._dispatcher = dispatcher;
    this._newsletterSubscribers = [];

    dispatcher.subscribe('INTERESTED_PERSON_CREATED', (event: Event): void => {
      this._newsletterSubscribers.push(event.payload);
    });
  }

  read = async (): Promise<NewsletterSubscriber[]> => {
    return this._newsletterSubscribers;
  };
}