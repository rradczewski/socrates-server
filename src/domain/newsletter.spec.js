// @flow

import Newsletter from './newsletter';
import DomainEventDispatcher from './domainEventDispatcher';
import NewsletterRepository from '../db/newsletterRepository';
import MySql from '../db/MySql';

jest.mock('../db/MySql');
import TestHelper from '../db/SqlTestHelper';
import type {NewsletterSubscriber} from './newsletter';

describe('Newsletter should', () => {
  const dispatcher = new DomainEventDispatcher();
  const testHelper = new TestHelper();
  let mySql, newsletter;
  beforeEach(() => {
    mySql = new MySql(testHelper.testConfig);
    newsletter = new Newsletter(new NewsletterRepository(mySql), dispatcher);

    mySql.insert.mockImplementation(() => {
      return Promise.resolve({
        numberOfRowsAffected: 1,
        lastInsertedId: 4711
      });
    });
  });

  describe('when signing up', () => {
    describe('and name and email are provided', () => {
      it('create an interested person', () => {
        return newsletter.signup('test', 'test@test.org')
          .then((person: ?NewsletterSubscriber) => {
            expect(person).toBeDefined();
            expect(mySql.insert.mock.calls.length).toBe(1);
          });
      });

      it('dispatch INTERESTED_PERSON_CREATED', async (done) => {
        dispatcher.subscribeOnce('INTERESTED_PERSON_CREATED', () => done());
        await newsletter.signup('test', 'test@test.org');
      });
    });

    describe('and input is invalid', () => {
      it('throw if name is empty', () => {
        return newsletter.signup('', 'name@domain.de')
          .then(() => {throw new Error('Promise should not resolve.');})
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid: The name is missing.');
            expect(mySql.insert.mock.calls.length).toBe(0);
          });
      });

      it('throw if email is empty', () => {
        return newsletter.signup('Name', '')
          .then(() => {throw new Error('Promise should not resolve.');})
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid: The email address is missing.');
            expect(mySql.insert.mock.calls.length).toBe(0);
          });
      });

      it('throw if email is invalid', () => {
        return newsletter.signup('Name', 'invalid email')
          .then(() => {throw new Error('Promise should not resolve.');})
          .catch((err: Error) => {
            expect(err.message).toEqual('The information provided is not valid: The email address is malformed.');
            expect(mySql.insert.mock.calls.length).toBe(0);
          });
      });
    });
  });
});