// @flow
import Aggregate from './aggregate';
import {EventDispatcher} from './eventDispatcher';
import {validateEmail} from '../utilities/Utilities';
import AuthenticationRepository from '../db/authenticationRepository';
import {compareSync} from 'bcrypt';
import {sign} from 'jsonwebtoken';

export type LoginResult = {
  token: string,
}

export type User = {
  name: string,
  email: string,
  password: string,
  isAdministrator: boolean
}
export type AccessCheckResult = {
  allowed: boolean,
  error: ?Error
}

export const JSONWebTokenSecret = '$lsRTf!gksTRcDWs';
const requestNeedsAuthentication = ['/profile'];
const requestNeedsAdministrator = ['/management'];

const needsAuthentication = (url: string): boolean => {
  const pattern = new RegExp(requestNeedsAuthentication.join('|'));
  return pattern.test(url.toLowerCase());
};
const needsAdministrator = (url: string): boolean => {
  const pattern = new RegExp(requestNeedsAdministrator.join('|'));
  return pattern.test(url.toLowerCase());
};

export default class Authenticator extends Aggregate {
  _repository: AuthenticationRepository;

  constructor(repository: AuthenticationRepository, dispatcher: EventDispatcher) {
    super(dispatcher);
    this._repository = repository;
  }

  _notifyLoginSuccess(user: User): LoginResult {
    this.dispatch({type: 'USER_LOGGED_IN', payload: user});
    const token = sign({
      email: user.email,
      name: user.name,
      isAdministrator: user.isAdministrator
    }, JSONWebTokenSecret);
    return {token: token};
  }

  _notifyLoginFailure(email: string): LoginResult {
    this.dispatch({type: 'USER_LOGIN_FAILURE', payload: email});
    return {token: ''};
  }

  async login(email: string, password: string): Promise<LoginResult> {
    if (!validateData(email, password)) {
      return this._notifyLoginFailure(email);
    }
    const user: ?User = await this._repository.readPersonByEmail(email);
    if (!user || !compareSync(password, user.password)) {
      return this._notifyLoginFailure(email);
    }
    return this._notifyLoginSuccess(user);
  }

  async checkAccess(url: string, determinedUser: ?User): Promise<AccessCheckResult> {
    const authenticationRequired = needsAuthentication(url);
    const administratorRequired = needsAdministrator(url);
    let theUser: ?User = null;
    if (determinedUser) {
      theUser = await this._repository.readPersonByEmail(determinedUser.email);
    }
    if (!theUser && (authenticationRequired || administratorRequired)) {
      return {allowed: false, error: new Error('Authentication required.')};
    }
    if (theUser && administratorRequired && !theUser.isAdministrator) {
      return {allowed: false, error: new Error('Administrator required.')};
    }

    return {allowed: true, error: null};
  }
}

function isEmpty(variable) {
  return !variable || variable === '';
}

function validateData(email: string, password: string) {
  return !(isEmpty(password) || isEmpty(email) || !validateEmail(email));
}