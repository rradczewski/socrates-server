// @flow
import type {Event, EventDispatcher, Listener} from './eventDispatcher';

export default class DomainEventDispatcher implements EventDispatcher {
  callbacks = {};

  callbacksOnce = {};

  // noinspection JSUnusedGlobalSymbols
  subscribe(type: string, listenerCallback: Listener) {
    DomainEventDispatcher._addListenerCallback(this.callbacks, type, listenerCallback);
  }

  subscribeOnce(type: string, listenerCallback: Listener) {
    DomainEventDispatcher._addListenerCallback(this.callbacksOnce, type, listenerCallback);
  }

  // noinspection JSUnusedGlobalSymbols
  unsubscribe(type: string, listenerCallback: Listener) {
    DomainEventDispatcher._removeListenerCallback(this.callbacks, type, listenerCallback);
    DomainEventDispatcher._removeListenerCallback(this.callbacksOnce, type, listenerCallback);
  }

  // noinspection JSUnusedGlobalSymbols
  dispatchEvent(event: Event) {
    if (this.callbacks.hasOwnProperty(event.type)) {
      this.callbacks[event.type].forEach(listenerCallback => listenerCallback(event));
    }
    if (this.callbacksOnce.hasOwnProperty(event.type)) {
      this.callbacksOnce[event.type].forEach(listenerCallback => listenerCallback(event));
      delete this.callbacksOnce[event.type];
    }
  }

  static _addListenerCallback(callbackContainer, type, listenerCallback) {
    if (!callbackContainer.hasOwnProperty(type)) {
      callbackContainer[type] = [];
    }
    callbackContainer[type].push(listenerCallback);
  }

  static _removeListenerCallback(callbackContainer, type, listenerCallback) {
    if (callbackContainer.hasOwnProperty(type)) {
      callbackContainer[type] = callbackContainer[type].filter(cb => cb !== listenerCallback);
    }
  }
}
