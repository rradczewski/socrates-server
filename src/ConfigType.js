// @flow
export type Config = {
  environment: string,
  database: {
    host: string,
    port: number,
    name: string,
    user: string,
    password: string,
    debug: boolean
  },
  server: {
    port: number;
  }
}

