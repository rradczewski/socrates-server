// @flow

import {validateEmail} from './Utilities';

describe('validateEmail should', () => {
  it('accept valid address', () => {
    expect(validateEmail('name@domain.de')).toBeTruthy();
  });

  it('accept german characters in local part', () => {
    expect(validateEmail('äö-üß.german@domain.de')).toBeTruthy();
  });

  it('accept subdomains', () => {
    expect(validateEmail('name@sub-domain.domain.de')).toBeTruthy();
  });

  it('refuse empty string', () => {
    expect(validateEmail('')).toBeFalsy();
  });

  it('refuse address with forbidden characters', () => {
    expect(validateEmail('name\\@domain.de')).toBeFalsy();
    expect(validateEmail('.name@domain.de')).toBeFalsy();
    expect(validateEmail('na,me@domain.de')).toBeFalsy();
    expect(validateEmail('name@äää.de')).toBeFalsy();
    expect(validateEmail('name@dom ain.äö')).toBeFalsy();
    expect(validateEmail('na me@domain.äö')).toBeFalsy();
  });

  it('refuse malformed address', () => {
    expect(validateEmail('name.domain.de')).toBeFalsy();
    expect(validateEmail('@name.domain.de')).toBeFalsy();
    expect(validateEmail('name.domain.de@')).toBeFalsy();
    expect(validateEmail('name@domain')).toBeFalsy();
    expect(validateEmail('name@domain.a')).toBeFalsy();
    expect(validateEmail('name@domain@domain.fr')).toBeFalsy();
  });
});