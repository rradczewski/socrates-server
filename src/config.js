// @flow

import rc from 'rc';
import type {Config} from './ConfigType';

// add your own config in .socrates-serverrc file and don't add it to git if you want to keep your secrets.

const config: Config = {
  'environment': 'dev',
  'database': {
    'host': '',
    'port': 0,
    'name': '',
    'user': '',
    'password': '',
    'debug': false
  },
  'server': {
    'port': 4444
  }
};
export default rc('socrates-server', config);