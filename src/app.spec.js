// @flow
import App from './app';
import axios from 'axios';

jest.mock('./db/MySql');
import MySql from './db/MySql';
import type Config from './config';
import DomainEventDispatcher from './domain/domainEventDispatcher';
import type {EventDispatcher} from './domain/eventDispatcher';
import createNewsletterServer from './routes/newsletterServer';
import rootServer from './routes/rootServer';
import defaultHeaders from './routes/defaultHeaders';
import bodyParsers from './routes/bodyParsers';
import type {QueryResult} from './db/MySql';
import authenticationMiddleware from './routes/authenticationMiddleware';

describe('Application', () => {
  let app: App, dispatcher: EventDispatcher;
  const defaultConfiguration: Config = {
    'environment': 'test',
    'database': {
      'host': 'localhost',
      'port': 3306,
      'name': 'socrates',
      'user': '',
      'password': ''
    },
    'server': {
      'port': 9999
    }
  };
  const sqlHandler = new MySql(defaultConfiguration);
  sqlHandler.initialize();

  beforeAll(async () => {
    dispatcher = new DomainEventDispatcher();
    app = new App(defaultConfiguration);
    app.applyMiddleware(bodyParsers);
    app.applyMiddleware(defaultHeaders);
    app.applyMiddleware(authenticationMiddleware(sqlHandler, dispatcher));
    app.applyMiddleware(createNewsletterServer(sqlHandler, dispatcher));
    app.applyMiddleware(rootServer);
    await app.run();
  });

  afterAll(async () => {
    if (app) {
      await app.stop();
    }
  });

  it('starts', () => {
    expect(app.isRunning).toBe(true);
  });

  it('accepts get requests to root', () => {
    return axios.get('http://localhost:9999/')
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  it('rejects requests to non existent urls', (done) => {
    axios.get('http://localhost:9999/non-existent')
      .catch(err => {
        expect(err.response.status).toBe(404);
        done();
      });
  });

  it('accepts api get requests to root', () => {
    return axios.get('http://localhost:9999/api/v1/')
      .then((response) => {
        expect(response.status).toBe(200);
      });
  });

  describe('accepts api get requests to interested-people', () => {
    // create a person first
    const person = {id: 'id', name: 'test', email: 'email'};

    it('returns empty data when no interested-people are present', () => {
      return axios.get('http://localhost:9999/api/v1/interested-people')
        .then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toEqual([]);
        });
    });

    it('returns list of persons when they are present', () => {
      dispatcher.dispatchEvent(
        {type: 'INTERESTED_PERSON_CREATED', payload: person});

      return new Promise(resolve => setTimeout(resolve, 100))
        .then(() => axios.get('http://localhost:9999/api/v1/interested-people'))
        .then(response => {
          expect(response.status).toBe(200);
          expect(response.data).toEqual([person]);
        });
    });
  });

  it('accepts api post requests to interested-people', () => {
    sqlHandler.insert.mockImplementation((): Promise<QueryResult> => Promise.resolve({
      numberOfRowsAffected: 1,
      lastInsertedId: 4711
    }));
    return axios.post('http://localhost:9999/api/v1/interested-people', {
      name: 'FirstName LastName',
      email: 'name@domain.de'
    }).then(response => {
      expect(response.status).toBe(200);
      expect(response.data).toBeTruthy();
    });
  });

  it('return 500 on failing post request to interested-people', () => {
    sqlHandler.insert.mockImplementation((): Promise<QueryResult> => Promise.resolve({
      numberOfRowsAffected: 1,
      lastInsertedId: 4711
    }));
    return axios.post('http://localhost:9999/api/v1/interested-people', {
      name: 'FirstName LastName',
      email: 'name@domain.de'
    })
      .catch((err) => {
        expect(err.response.status).toBe(500);
        expect(err.response.data).toEqual({});

      });
  });

  it('returns 500 code and empty data on error by a post request to interested-people', (done) => {
    sqlHandler.insert.mockImplementation((): Promise<boolean> => Promise.reject(new Error()));
    axios.post('http://localhost:9999/api/v1/interested-people', {
      name: 'FirstName LastName',
      email: 'name@domain.de'
    }).catch(error => {
      expect(error.response.status).toBe(500);
      expect(error.response.data).toEqual({});
      done();
    });
  });
});
