// @flow
import App from './app';
import config from './config';
import rootServer from './routes/rootServer';
import createNewsletterServer from './routes/newsletterServer';
import MySql from './db/MySql';
import DomainEventDispatcher from './domain/domainEventDispatcher';
import defaultHeaders from './routes/defaultHeaders';
import bodyParsers from './routes/bodyParsers';
import authenticationMiddleware from './routes/authenticationMiddleware';

const sqlHandler = new MySql(config);
sqlHandler.initialize();
const dispatcher = new DomainEventDispatcher();

const app = new App(config);

app.applyMiddleware(bodyParsers);
app.applyMiddleware(defaultHeaders);
app.applyMiddleware(authenticationMiddleware(sqlHandler, dispatcher));
app.applyMiddleware(createNewsletterServer(sqlHandler, dispatcher));
app.applyMiddleware(rootServer);

app.run().catch(err => console.log(err));
