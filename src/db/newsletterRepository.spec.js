// @flow

import MySql from './MySql';
import TestHelper from './SqlTestHelper';
import NewsletterRepository from './newsletterRepository';
import type {NewsletterSubscriber} from '../domain/newsletter';
jest.mock('./MySql');

let interestedPeopleHandler: NewsletterRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Newsletter repository should', () => {

  beforeEach(() => {
    interestedPeopleHandler = new NewsletterRepository(mySql);
  });

  it('read all interested people', () => {
    mySql.select.mockImplementation(() => {
      return Promise.resolve([]);
    });
    return interestedPeopleHandler.readAll()
      .then((interestedPeople: NewsletterSubscriber[]) => {
        expect(interestedPeople.length).toBe(0);
        expect(mySql.select.mock.calls.length).toBe(1);
      });
  });

  it('add one interested person', () => {
    mySql.insert.mockImplementation(() => {
      return Promise.resolve({
        numberOfRowsAffected: 1,
        lastInsertedId: 4711
      });
    });
    return interestedPeopleHandler.addPerson('FirstName LastName', 'name@domain.de')
      .then((person: ?NewsletterSubscriber) => {
        expect(person).toBeDefined();
        expect(mySql.insert.mock.calls.length).toBe(1);
      });
  });
});