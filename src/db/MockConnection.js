export default class MockConnection {

  items = [];
  queryReturnValue = null;
  error = null;

  query(command, params, callback) {
    if(!callback) {
      callback = params;
      return callback(this.error, this.items);
    } else {
      const insertedItem = {id: params[0], name: params[1], email: params[2]};
      this.items.push(insertedItem);
      return callback(this.error, this.queryReturnValue);
    }
  }

  release() {
  }

}

