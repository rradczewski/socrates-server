// @flow

import MySql from './MySql';
import TestHelper from './SqlTestHelper';
import AuthenticationRepository from './authenticationRepository';
import type {User} from '../domain/authenticator';

jest.mock('./MySql');

let authRepository: AuthenticationRepository;
const testHelper = new TestHelper();
const mySql = new MySql(testHelper.testConfig);

describe('Authentication repository', () => {

  beforeEach(() => {
    authRepository = new AuthenticationRepository(mySql);
  });

  describe('if the email is available', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve(
          [
            {name: 'test', email: 'email@valid.de', password: 'theHash'},
            {name: 'test2', email: 'another@valid.de', password: 'theHash2'}
          ]
        );
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('then it finds the user', () => {
      return authRepository.readPersonByEmail('email@valid.de')
        .then((user: ?User) => {
          expect(user).toBeDefined();
          expect(mySql.select.mock.calls.length).toBe(1);
        });
    });
    it('then the found user has the requested email', () => {
      return authRepository.readPersonByEmail('email@valid.de')
        .then((user: ?User) => {
          if (user) {
            expect(user.email).toEqual('email@valid.de');
          }
        });
    });
  });
  describe('if the email is NOT available', () => {
    beforeEach(() => {
      mySql.select.mockImplementation(() => {
        return Promise.resolve(
          [
            {name: 'test', email: 'myname@example.com', password: 'theHash'},
            {name: 'test2', email: 'another@valid.de', password: 'theHash2'}
          ]
        );
      });
    });
    afterEach(() => {
      mySql.select.mockReset();
    });

    it('then no user is found', () => {
      return authRepository.readPersonByEmail('email@valid.de')
        .then((user: ?User) => {
          expect(user).toBeNull();
          expect(mySql.select.mock.calls.length).toBe(1);
        });
    });
  });
});