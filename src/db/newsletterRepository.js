// @flow
import MySql from './MySql';
import type {NewsletterSubscriber} from '../domain/newsletter';
import type {QueryResult} from './MySql';

export default class NewsletterRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async addPerson(name: string, email: string): Promise<?NewsletterSubscriber> {
    const addNewSubscriberSql = 'INSERT INTO person (nickname, email, is_newsletter_subscriber) VALUES (?, ?, 1);';
    const values = [name, email];
    const result: QueryResult = await this._mySql.insert(addNewSubscriberSql, values);
    if (result.numberOfRowsAffected === 1) {
      return {name, email};
    }
  }

  async readAll(): Promise<NewsletterSubscriber[]> {
    const readAllSubscribersSql = 'SELECT * FROM person WHERE is_newsletter_subscriber = 1;';
    const people: [] = await this._mySql.select(readAllSubscribersSql);
    return people.map(p => (p: NewsletterSubscriber));
  }
}