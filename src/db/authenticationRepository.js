// @flow
import MySql from './MySql';
import type {User} from '../domain/authenticator';

export default class AuthenticationRepository {

  _mySql: MySql;

  constructor(mySql: MySql) {
    this._mySql = mySql;
  }

  async readPersonByEmail(email: string): Promise<?User> {
    /*
    const readPersonSql = 'SELECT id FROM person WHERE email = ?;';
    const values = [email];
    const results = await this._mySql.selectWithParams(readPersonSql, values);
    */
    const readPersonSql = 'SELECT * FROM person;';
    const results: [] = await this._mySql.select(readPersonSql);
    const userData = results.find((item) => item.email === email);
    if (userData) {
      return {
        name: userData.nickname,
        email: userData.email,
        password: userData.password,
        isAdministrator: userData.is_administrator
      };
    }
    return null;
  }
}