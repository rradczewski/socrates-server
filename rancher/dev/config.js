// @flow
/* eslint-disable */
import type {Config} from '../../src/ConfigType';

const config: Config = {
  'environment': 'dev',
  'database': {
    'host': 'socrates-db',
    'port': 3306,
    'name': 'socrates_db',
    'user': '%username%',
    'password': '%password%',
    'debug': false
  },
  'server': {
    'port': 4444
  }
};
export default config;