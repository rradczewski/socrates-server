# A SoCraTes Hello-World Server

### Issues

We keep all issues in the [client repository](https://gitlab.com/socrates-conference/socrates-client/issues). Please use the right tag to specify if the issue refers to the server, client or both repositories.

### Requirements

- NodeJS v8.x (If you're on Linux/Mac, We strongly recommend using [NVM](https://github.com/creationix/nvm) to manage your node installations. There also is an [NVM for Windows](https://github.com/coreybutler/nvm-windows), but that's a different project - no guarantees. )
- [Yarn](https://yarnpkg.com/) (`npm install -g yarn`)
- flow-typed (`npm install -g flow-typed && flow-typed install jest`)
- `npm install -g flow-typed && flow-typed install jest@21.x.x` (should match jest version used in the project)

### Summary

The server for our little Hello World project consists of three layers: 
1. The REST endpoints, implemented using an [expressjs](http://expressjs.com) middleware
2. A domain core, consisting of a single Aggregate ('Messages').
3. A repository, implemented in-memory (a simple array of messages).


### Run the tests

```
yarn test
```

### First run: Setting up the database
We use ```db-migrate``` to properly keep our database in order. 
It must be installed globally on your dev machine, but it will choose any local version from node_modules later:
```
npm install -g db-migrate
``` 
For the application to run out of the box, you must have a running local instance of MySQL with the default port 3306 open, create a database called "socrates_db" with a user that has all privileges, and set the following environment variables:
``` 
export DB_ENV=local
export DB_USER=${your_local_db_user}
export DB_PASS=${your_local_db_password}
```
and the run the following command:
```
db-migrate up --env=$DB_ENV  
```
Or optionally, you may also pass these variables without exporting them:
```
DB_USER=${your_local_db_user} DB_PASS=${your_local_db_password} db-migrate up --env=local   
```
In order to use a different port, you may modify ```database.json``` - but **please be sure not to commit your changes to the repository!** 

### Modifying the database

Again: We use ```db-migrate``` to properly keep our database in order.  
It is fairly simple to use:
1. Create a new migration with ```db-migrate create my-migration-name```
This will create an empty SQL file for the UP and DOWN migration paths, and a JavaScript file that will read them.
2. Add your desired changes to the UP file, using common SQL syntax (e.g., ```ALTER TABLE```, or ```CREATE TABLE``` go there).
3. Make sure to add a reverse migration path to the DOWN file (e.g., for every ```CREATE TABLE``` in UP, there should be a ```DROP TABLE``` here).
4. Run ```db-migrate up --env=local``` to check that your local db reflects the desired changes.

### Run the local server

```
yarn start
```

### Run production build

```
yarn build
yarn serve
```

Note: ```yarn serve``` will execute the DB migrations as described above, but here the environment variables matter: 
For everything to run out of the box, you must have a running local instance of MySQL with the default port 3306 open, create a database called "socrates_db" with a user that has all privileges, and set the following environment variables:
``` 
export DB_ENV=local
export DB_USER=${your_local_db_user}
export DB_PASS=${your_local_db_password}
```
